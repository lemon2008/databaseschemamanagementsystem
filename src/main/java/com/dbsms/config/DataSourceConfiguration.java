package com.dbsms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {
	
	 @Bean
	    public DataSource dataSource() {
	        final JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
	        jndiDataSourceLookup.setResourceRef(true);
	        DataSource dataSource = jndiDataSourceLookup.getDataSource("java:/OracleDS");
	        return dataSource;
	    }

}
