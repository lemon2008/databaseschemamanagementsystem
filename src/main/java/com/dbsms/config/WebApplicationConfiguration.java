package com.dbsms.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

// This configuration file along with the others in this directory
// take over for route-servlet.xml and persistence.xml
// See the file HibernateConfiguration.java for the Hibernate configuration
// The Component Scan annotation tells spring to scan for Spring managed
// components at the com.dbsms package and below.
// The annotation Component Scan ("com.dbsms") is the
// same as <context:component-scan base-package="com.dbsms" />

@Configuration
@ComponentScan("com.dbsms")
@EnableWebMvc
public class WebApplicationConfiguration extends WebMvcConfigurerAdapter  {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}