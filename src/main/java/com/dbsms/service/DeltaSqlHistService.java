package com.dbsms.service;

import com.dbsms.model.DeltaSqlHist;

import java.util.List;

/**
 * Created by david on 12/10/15.
 */
public interface DeltaSqlHistService {
    List<DeltaSqlHist> findAll();

    void saveDeltaSqlHist(DeltaSqlHist deltaSqlHist);

    void applyChange(String deltaSql);
}
