package com.dbsms.service;

/**
 * Created by david on 12/10/15.
 */
public interface DeltaSqlHistFileService {
    void appendToFile(String sqlToAppend, String message);
}
