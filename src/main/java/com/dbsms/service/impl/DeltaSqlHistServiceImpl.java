package com.dbsms.service.impl;

import com.dbsms.dao.DeltaSqlHistDao;
import com.dbsms.dao.DeltaSqlHistRepository;
import com.dbsms.model.DeltaSqlHist;
import com.dbsms.service.DeltaSqlHistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by david on 12/10/15.
 */
@Service("deltaSqlHistService")
public class DeltaSqlHistServiceImpl implements DeltaSqlHistService {
    @Autowired
    private DeltaSqlHistDao deltaSqlHistDao;

    @Autowired
    private DeltaSqlHistRepository deltaSqlHistRepository;

    @Override
    public List<DeltaSqlHist> findAll() {
        return deltaSqlHistDao.findAll();
    }

    @Override
    public void saveDeltaSqlHist(DeltaSqlHist deltaSqlHist) {
        deltaSqlHistRepository.save(deltaSqlHist);
    }

    @Override
    public void applyChange(String deltaSql) {
        deltaSqlHistDao.applyChange(deltaSql);
    }
}
