package com.dbsms.dao.jdbc;

import com.dbsms.dao.DeltaSqlHistDao;
import com.dbsms.model.DeltaSqlHist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by david on 12/9/15.
 */
@Repository
public class DeltaSqlHistDaoJdbc implements DeltaSqlHistDao {
    /**
     * Instance of the logger class.
     */
    private final static Logger logger = LoggerFactory.getLogger(DeltaSqlHistDaoJdbc.class);

    /**
     * Spring auto wired data source
     */
    @Autowired
    private DataSource jndiDataSource;


    @Override
    public List<DeltaSqlHist> findAll() {
        JdbcTemplate jdbcTemplateObject = new JdbcTemplate(jndiDataSource);
        String sql = "select * from \"$$_DELTA_SQL_HIST\"";
        return jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(DeltaSqlHist.class));
    }

    @Override
    public void applyChange(String deltaSql) {
        JdbcTemplate jdbcTemplateObject = new JdbcTemplate(jndiDataSource);
        jdbcTemplateObject.execute(deltaSql);
    }
}
