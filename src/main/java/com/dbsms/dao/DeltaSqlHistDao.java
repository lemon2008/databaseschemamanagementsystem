package com.dbsms.dao;

import com.dbsms.model.DeltaSqlHist;

import java.util.List;

/**
 * Created by david on 12/9/15.
 */
public interface DeltaSqlHistDao {

    List<DeltaSqlHist> findAll();

    void applyChange(String deltaSql);
}
