package com.dbsms.dao;

import com.dbsms.model.DeltaSqlHist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by david on 12/14/15.
 */
public interface DeltaSqlHistRepository extends JpaRepository<DeltaSqlHist, String> { }