package com.dbsms.controller;

import com.dbsms.model.DeltaSqlHist;
import com.dbsms.service.DeltaSqlHistFileService;
import com.dbsms.service.DeltaSqlHistService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class DbsmsController {

    private static final String APPLY_DELTA_SQL_ON = "on";

	private static final Logger logger = LoggerFactory
			.getLogger(DbsmsController.class);

    @Autowired
    private DeltaSqlHistService deltaSqlHistService;

    @Autowired
    private DeltaSqlHistFileService deltaSqlHistFileService;


    @RequestMapping(value="/deltaSqlHist", method= RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<DeltaSqlHist> deltaSqlHist() {
        return deltaSqlHistService.findAll();
    }

	@RequestMapping(value = "/home")
	public String home(ModelMap modelMap) {
		modelMap.addAttribute("pageTitle", "Database Schema Management System");
		return "Home";
	}

    @RequestMapping(value = "/apply")
    public String apply(ModelMap modelMap) {
        modelMap.addAttribute("pageTitle", "Database Schema Management System");
        return "Apply";
    }

    @RequestMapping(value = "/applyDelta", method = RequestMethod.POST)
    public String applyDelta(RedirectAttributes redirectAttributes, HttpServletRequest request) {
        String changeId = request.getParameter("changeId");
        logger.info("changeId: " + changeId);
        String appliedDate = request.getParameter("appliedDate");
        logger.info("appliedDate: " + appliedDate);
        String dbUser = request.getParameter("dbUser");
        logger.info("dbUser: " + dbUser);
        String osUser = request.getParameter("osUser");
        logger.info("osUser: " + osUser);
        String deltaAuthor = request.getParameter("deltaAuthor");
        logger.info("deltaAuthor: " + deltaAuthor);
        String target = request.getParameter("target");
        logger.info("target: " + target);
        String comments = request.getParameter("comments");
        logger.info("comments: " + comments);
        String deltaSQL = request.getParameter("deltaSQL");
        logger.info("deltaSQL: " + deltaSQL);
        String apply = request.getParameter("apply");
        logger.info("apply: " + apply);

        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Date appliedDateDate = null;
        try {
            appliedDateDate = format.parse(appliedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DeltaSqlHist deltaSqlHist = new DeltaSqlHist(changeId, appliedDateDate, dbUser, osUser, deltaAuthor,
                 comments, target, deltaSQL, apply);

        postDelta(deltaSqlHist);

        return "redirect:/home";
    }

    @RequestMapping(value = "/postDelta", method = RequestMethod.POST, consumes = "application/json", headers = {"Content-type=application/json"})
    @ResponseBody
    public ResponseEntity<DeltaSqlHist> postDelta(@RequestBody DeltaSqlHist deltaSqlHist) {
        logger.info(deltaSqlHist.toString());

        deltaSqlHistService.saveDeltaSqlHist(deltaSqlHist);

        logger.info("Add the change to the Delta SQL file.");

        /**
         * -- Change label:
         --------------------------------------------------------------------------
         -- Date: MM/DD/YYYY
         -- Reason:
         -- Name:
         --------------------------------------------------------------------------
         -- BEGIN CHANGE *** This line marks the start of a change.


         -- END CHANGE *** This line marks the end of a change.
         */
        String endComment = "\n";
        String deltaSqlToAppend = "\n\n\n"
                + "-- Change label:  " + deltaSqlHist.getChangeId() + "\n"
                + "--------------------------------------------------------------------------" + "\n"
                + "-- Date:  " + deltaSqlHist.getAppliedDate() + "\n"
                + "-- Reason:  "+ deltaSqlHist.getComments() + "\n"
                + "-- Name:  " + deltaSqlHist.getDeltaAuthor() + "\n"
                + "--------------------------------------------------------------------------" + "\n"
                + "-- BEGIN CHANGE *** This line marks the start of a change."
                + "\n\n"
                + deltaSqlHist.getDeltaSQL()
                + "\n\n"
                + "-- END CHANGE *** This line marks the end of a change." + "\n";
        String commitMessage = "Commit change: " + deltaSqlHist.getChangeId();
        deltaSqlHistFileService.appendToFile(deltaSqlToAppend, commitMessage);


        if (deltaSqlHist.getApply().equals(APPLY_DELTA_SQL_ON)) {
            logger.info("Applying the change.");
            deltaSqlHistService.applyChange(deltaSqlHist.getDeltaSQL().replaceAll(";", ""));
        }




        ResponseEntity<DeltaSqlHist> responseEntity =
                new ResponseEntity<DeltaSqlHist>(
                        deltaSqlHist, HttpStatus.CREATED);
        return responseEntity;
    }


}
