function changeColor(element){
	document.getElementById(element).style.background="#698595";
	document.getElementById(element).style.color="#ffffff";
}
function changeColorToNormal(element){
	document.getElementById(element).style.background="#ffffff";
	document.getElementById(element).style.color="#000";
}
function getAjaxLookupList(url, entityAttribute, htmlInputFieldId, lookUpDiv){
	if(jQuery.trim(jQuery("#"+htmlInputFieldId).val()) == ""){
		jQuery("#"+lookUpDiv).css("display","none");
		return false;
	}
	jQuery("#"+lookUpDiv).css("display","");
	jQuery("#"+lookUpDiv).html("<img src='https://s3.amazonaws.com/static.dbsms.net/loading.gif' style='margin-top:10px;'/>");
    var json = entityAttribute+"=" + jQuery.trim(jQuery("#"+htmlInputFieldId).val());
    jQuery.ajax({
        url: url,
        type: 'POST',
        data: json,
        cache:false,
        success:function(response){
        	var lookup_string = "<ul style=\"padding:0px;margin:0;\">";
        	var count = 0;
            jQuery.each(response, function(key, element) {
    			lookup_string += "<li style=\"cursor:pointer;height:16px;overflow:hidden;background:none repeat scroll 0% 0% rgb(255, 255, 255);color:rgb(0, 0, 0);\" ";
    		    lookup_string += "name=\""+key+"\" id=\""+key+"\" onmouseover=\"return changeColor(this.id);\" title=\""+element+"\" onmouseout=\"return changeColorToNormal(this.id);\" ";
    		    lookup_string += "onclick=\"return pickEntity('"+element.replace(/'/g, "\\'")+"','"+key+"', '"+lookUpDiv+"');\">"+element+"</li>";
    		    count ++;
			});
            if(count == 0){
            	lookup_string += "<li style=\"cursor:pointer;height:16px;overflow:hidden;background:none repeat scroll 0% 0% rgb(255, 255, 255);color:rgb(0, 0, 0);\">No record found</li>";
            }
            lookup_string += "</ul>";
            jQuery("#"+lookUpDiv).css("text-align","left");
            jQuery("#"+lookUpDiv).html(lookup_string);
        },
        error:function(jqXhr, textStatus, errorThrown){
        }
    });
    return true;
}
function sort(element){
	var sel = jQuery("#"+element);
	var opts_list = sel.find('option');
	opts_list.sort(function(a, b) { return jQuery(a).text() > jQuery(b).text(); });
	sel.html("<option value=\"0\">Select</option>").append(opts_list);
	jQuery("#"+element).val("0");
}
function printdiv()
{
	var newstr = document.getElementById("container").innerHTML;
	var oldstr = document.body.innerHTML;
	document.body.innerHTML = newstr;
	window.print();
	document.body.innerHTML = oldstr;
	return false;
}