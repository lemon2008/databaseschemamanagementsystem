<%@include file="includes/header.jsp" %>
<style type="text/css">
	table#alternate_coloring tr:nth-child(even)	{
		background-color:#f1f4f9;
	}
	table#alternate_coloring tr:nth-child(odd) {
	  	background-color:#ffffff;
	}
	table#alternate_coloring tr.table_header_row {
		background-color:#f1f1f1;
	}
	table#alternate_coloring tr.empty_row {
		background-color:#color:#838383;
		height: 0px;
	}
	table th {
		 cursor: pointer;
		 background-color:#f1f1f1;
		 color: black;
		 text-align: left;
	}
	table tr {
		height: 28px;
	}
	table td {
		padding: 0px;
	}
	
	.md-input-container {
  		margin-right: 20px; 
  	}

</style>

<script>
	var app = angular.module('StarterApp', ['ngMaterial']);
    var selectedIndex = 0;
	app.controller('financials', function ($scope, $http) {
		$scope.get_delta = function() {
            jQuery("#feedback").html("Getting Delta SQL History...");
            $http({
                url: '<c:url value="/deltaSqlHist" />',
                method: 'get'
            }).success(function (data) {
                $scope.delta_response = data;
                jQuery("#feedback").html("");
                jQuery("#feedback").css("display", "none");
            });

        }

        $scope.newdelta = {
            submit : function() {
                if ($scope.deltaForm.$invalid) {
                    return false;
                }
                submitUpdate();
            }
        }

        $scope.selectedIndex = 0;

        $scope.changeTab = function() {
            $scope.selectedIndex = 0;
            $('#tabs').tabs("option","md-selected",0);
        }
    });

    function submitUpdate() {
        var changeId = jQuery("#changeId").val();
        var appliedDate = jQuery("#appliedDate").val();
        var dbUser = jQuery("#dbUser").val();
        var osUser = jQuery("#osUser").val();
        var deltaAuthor = jQuery("#deltaAuthor").val();
        var target = jQuery("#target").val();
        var comments = jQuery("#comments").val();
        var deltaSQL = jQuery("#deltaSQL").val();
        var apply = jQuery("#apply").val();
        jQuery
                .ajax({
                    url : '${pageContext.request.contextPath}/applyDelta',
                    type : 'POST',
                    dataType : 'html',
                    data : {
                        changeId : changeId,
                        appliedDate : appliedDate,
                        dbUser : dbUser,
                        osUser : osUser,
                        deltaAuthor : deltaAuthor,
                        target : target,
                        comments : comments,
                        deltaSQL : deltaSQL,
                        apply : apply
                    },
                    beforeSend : function() {
                        jQuery("feedback").html("Saving Delta...");
                    },
                    success : function() {
                        $( "#tabs" ).tabs({ active: 0 });
                        $scope.selectedIndex = 0;
                        changeTab();
                        get_delta();
                    }
                });
    }


function user_analysis(userid) 
{
	jQuery("#resource").css("display", "none");
	jQuery("#user_analysis").css("display", "");
	angular.element(jQuery("#revenue")).scope().userAnalysis(userid);
}

function resource_view() 
{
	jQuery("#user_analysis").css("display", "none");
	jQuery("#resource").css("display", "");
}

</script>

<div id="revenue" ng-controller="financials" layout="column" ng-cloak class="md-inline-form">
	<md-content layout-padding layout="row" layout-sm="column" style="width:100%;padding:0px;">
		<md-tabs name="tabs" id="tabs" md-selected="selectedIndex" md-dynamic-height md-border-bottom style="width:100%;display:block;position:relative;">
			<md-tab md-on-select="get_delta()" label="delta" md-selected="selectedIndex">
				<md-content class="md-padding">
					<div id="resource">
						<div id="app-container">
							<div><h2>Delta SQL History</h2></div>
							<div class="clear"></div>
							<div class="page_title_style"></div>
							<div class="clear"></div>
							<table id="alternate_coloring" class="responsive-table" align="center">
								<thead>
									<tr class="table_header_row">
										<td><b>Change Id</b></td>
										<td><b>Applied Date</b></td>
										<td><b>DB User</b></td>
										<td><b>OS User</b></td>
										<td><b>Delta Author</b></td>
                                        <td><b>Comments</b></td>
                                        <td><b>Target</b></td>
									</tr>
								</thead>
								<tbody>
									<tr class="empty_row"><td colspan="10" style="padding: 1px;"></td></tr>
									<tr ng-repeat="x in delta_response">
										<td>{{x.changeId}}</td>
										<td>{{x.appliedDate | date:'yyyy-MM-dd HH:mm:ss'}}</td>
										<td>{{x.dbUser}}</td>
                                        <td>{{x.osUser}}</td>
										<td>{{x.deltaAuthor}}</td>
                                        <td>{{x.comments}}</td>
                                        <td>{{x.target}}</td>
									</tr>		
								 </tbody>
							</table><br/>
						</div>
					</div>
				</md-content>
			</md-tab>
			<md-tab  md-on-select="get_setup()" label="New Delta" md-selected="selectedIndex">
				<md-content class="md-padding">
                        <form name="deltaForm" ng-submit="deltaForm.$valid && newdelta.submit()">
                            <span class="feedback" id="feedback"
                                  style="margin-left: 285px; padding: 0px;"></span>
                            <table cellspacing="0" cellpadding="4" border="0"
                                   style="float: left; width: 100%;">
                                <tr>
                                    <td>
                                        <md-input-container>
                                            <label style="text-align:left;">Change Id</label>
                                            <input type="text" id="changeId" name="changeId"
                                                   ng-model="changeId" ng-maxlength="20" required  style="float: left; width: 15%;"/>
                                        </md-input-container>

                                        <md-input-container>
                                            <label style="text-align:left;">Applied Date</label>
                                            <input type="date" id="appliedDate" name="appliedDate" ng-model="appliedDate"
                                                   style="float: left; width: 15%;"/>
                                        </md-input-container>


                                        <md-input-container>
                                            <label style="text-align:left;">DB User</label>
                                            <input type="text" id="dbUser" name="dbUser"
                                                   ng-model="dbUser" ng-maxlength="15"  style="float: left; width: 15%;"/>
                                        </md-input-container>

                                        <md-input-container>
                                            <label style="text-align:left;">OS User</label>
                                            <input type="text" id="osUser" name="osUser"
                                                   ng-model="osUser" ng-maxlength="15" style="float: left; width: 15%;" />
                                        </md-input-container>

                                        <md-input-container>
                                            <label style="text-align:left;">Delta Author</label>
                                            <input type="text" id="deltaAuthor" name="deltaAuthor"
                                                   ng-model="deltaAuthor" ng-maxlength="15"  style="float: left; width: 15%;"/>
                                        </md-input-container>

                                        <md-input-container>
                                            <label style="text-align:left;">Target</label>
                                            <input type="text" id="target" name="target"
                                                   ng-model="target" ng-maxlength="30"  style="float: left; width: 15%;"/>
                                        </md-input-container>


                                        <md-input-container class="md-block">
                                            <label>Comments</label>
                                            <textarea id="comments" name="comments" ng-model="comments" columns="1" md-maxlength="1000" rows="5"></textarea>
                                        </md-input-container>

                                        <md-input-container class="md-block">
                                            <label>Delta SQL</label>
                                            <textarea id="deltaSQL" name="deltaSQL" ng-model="deltaSQL" columns="1" md-maxlength="5000" rows="5"></textarea>
                                        </md-input-container>

                                        <md-input-container>
                                            <label style="text-align:left;">Apply</label>
                                            <input type="checkbox" id="apply" name="apply"
                                                   ng-model="apply"   style="float: left;"/>
                                        </md-input-container>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">
                                        <section layout="row" layout-sm="column"
                                                 layout-align="center center" layout-wrap>
                                            <md-button class="md-raised md-primary">Submit</md-button>
                                        </section>
                                    </td>
                                </tr>
                            </table>
                        </form>
				</md-content>
			</md-tab>	
		</md-tabs>
	</md-content>
</div>			<!-- Controller div -->
<%@include file="includes/footer.jsp" %>