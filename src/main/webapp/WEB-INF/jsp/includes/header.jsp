<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en" ng-app="StarterApp">
<head>
	<title>${pagetitle}</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/ico" href='<c:url value="/resources/images/favicon.ico" />'>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0-rc1/angular-material.min.css">
	<link rel="stylesheet" href='<c:url value="/resources/css/custom_material.css" /> '>
	<link href="css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
	
	<!-- Angular Material Dependencies -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-animate.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-aria.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0-rc1/angular-material.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	
	<style>
		.tabsdemoDynamicHeight md-content
		{
			background-color: transparent !important;
		}
		.tabsdemoDynamicHeight md-content md-tabs
		{
			background: #f6f6f6;
			border: 1px solid #e1e1e1;
		}
		.tabsdemoDynamicHeight md-content md-tabs md-tabs-wrapper
		{
			background: white;
		}
		.tabsdemoDynamicHeight md-content h1:first-child
		{
			margin-top: 0;
		}
		
		md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]) {
		color: #fff !important;
		}
	
		md-tabs > md-tabs-wrapper {
			background-color: #6EBEDF !important;
			border: 4px solid #e4e4e4  !important;
			border-width: 1px !important;
		}

		.md-padding {
			padding: 5px;
		}
	</style>
</head>

<body>
	<div id="app-main">
